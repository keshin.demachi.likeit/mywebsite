package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Update;
import dao.HairDao;

/**
 * Servlet implementation class HairSearchServlet
 */
@WebServlet("/HairSearchServlet")
public class HairSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HairSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		int gender =Integer.parseInt(request.getParameter("gender"));
		int length =Integer.parseInt(request.getParameter("length"));
		int f_type=Integer.parseInt(request.getParameter("f_type"));
		int f_color =Integer.parseInt(request.getParameter("f_color"));
		int neck =Integer.parseInt(request.getParameter("neck"));
		try {


			HairDao hairDao = new HairDao();
			Update sea = hairDao.Search(gender,length,f_type,f_color,neck);



			request.setAttribute("sea", sea);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/search.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
