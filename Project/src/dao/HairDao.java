package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.Diagnosis;
import beans.Update;

public class HairDao {
	public Diagnosis Diagnosis(Diagnosis dgs, int id) throws SQLException {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT t.gender, t.hair_name, t.hair_length, t.face_color, t.face_type, t.neck, t.img_id, t1.id, t1.img_name FROM hair t  INNER JOIN img t1 ON t.img_id = t1.id WHERE t.gender=? and  t.hair_length=? and t.face_type=? and t.face_color=? and t.neck=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, dgs.getGender());
			pStmt.setInt(2, dgs.getH_length());
			pStmt.setInt(3, dgs.getF_type());
			pStmt.setInt(4, dgs.getF_color());
			pStmt.setInt(5, id);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String h_name = rs.getString("hair_name");
			int gender = rs.getInt("gender");
			int h_length = rs.getInt("hair_length");
			int f_type = rs.getInt("face_type");
			int f_color = rs.getInt("face_color");
			String img = rs.getString("img_name");

			return new Diagnosis(h_name, gender, h_length, f_type, f_color, img);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public Update Search(int gender, int length, int f_type, int f_color, int neck) throws SQLException {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT t.gender, t.hair_name, t.hair_length, t.face_color, t.face_type, t.neck, t.img_id, t1.id, t1.img_name FROM hair t  INNER JOIN img t1 ON t.img_id = t1.id WHERE t.gender=? and  t.hair_length=? and t.face_type=? and t.face_color=? and t.neck=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, gender);
			pStmt.setInt(2, length);
			pStmt.setInt(3, f_type);
			pStmt.setInt(4, f_color);
			pStmt.setInt(5, neck);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}

			String h_name2 = rs.getString("hair_name");
			int gender2 = rs.getInt("gender");
			int h_length2 = rs.getInt("hair_length");
			int f_type2 = rs.getInt("face_type");
			int f_color2 = rs.getInt("face_color");
			String img2 = rs.getString("img_name");
			int img_id2 = rs.getInt("img_id");
			return new Update(h_name2, gender2, h_length2, f_type2, f_color2, img2, img_id2);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
		public void Name_Update(String h_name, int img_id) {
			Connection con = null;

			PreparedStatement stmt = null;

			try {
				con = DBManager.getConnection();
				String iSQL = "UPDATE hair SET hair_name =? WHERE img_id =?" ;
				stmt = con.prepareStatement(iSQL);
				stmt.setString(1, h_name);
				stmt.setInt(2, img_id);

				int r = stmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}
		}

		public void hair_Update(String image_name, int img_id) {
			Connection con = null;

			PreparedStatement stmt = null;

			try {
				con = DBManager.getConnection();
				String iSQL = "UPDATE img SET img_name = ? WHERE id =? ;" ;
				stmt = con.prepareStatement(iSQL);
				stmt.setString(1, image_name);
				stmt.setInt(2, img_id);

				int r = stmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}
		}
	}

