package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class Userdao {
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("user_name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public User miss(String loginId) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("user_name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public void Insert(String loginId, String password, String username, String date, String gender) {
		Connection conn = null;

		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			String iSQL = "INSERT INTO user(login_id,user_name,birth_date,password,user_gender) VALUES(?,?,?,?,?)";
			stmt = conn.prepareStatement(iSQL);
			stmt.setString(1, loginId);
			stmt.setString(2, username);
			stmt.setString(3, date);
			stmt.setString(4, password);
			stmt.setString(5, gender);
			int r = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}
	public void Delete(String login_id) {
		Connection conn = null;

		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			String iSQL = "DELETE FROM user WHERE login_id = ?;";
			stmt = conn.prepareStatement(iSQL);
			stmt.setString(1, login_id);

			int r = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}
	public String Cipher(String password) {
		String result="";

try {
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
		 result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
}catch(Exception e) {
	System.out.print(e);
}return result;
}
}
