<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="./css/test.css">
<link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" href="./css/index.css">
        <link rel="stylesheet" href="./css/header.css">
</head>

    <header>
        <h1 class="headline">
            <a class="">H.D.S ~Hair Diagnosis Site~</a>
        </h1>
        <ul class="nav-list">
            <li class="nav-list-item">

            </li>

            <a href="LogoutServlet" class="btn-flat-horizontal-border">
                <span>ログアウト</span>
            </a>
        </ul>
    </header>
    <hr>
    <div align="center">
        <table border="0">

        </table>
    </div>
    <form action="HairSearchServlet" method="get" >
   <section class="card">
  <div class="card-content">
    <h1 class="card-title">検索画面</h1>
    <ul>
    <li class="gender">
        <label for="gender">■性別</label>
        <input id="gender" type="radio" name="gender" value="1">男性
        <input id="gender" type="radio" name="gender" value="2">女性
    </li>
    <li class="length">
        <label for="length">■長さ</label>
        <select name="length">
        <option value="3">ショート</option>
        <option value="2">ミディアム</option>
        <option value="1">ロング</option>

        </select>
        </li>
        <li class="f_type">
        <label for="f_type">■顔の形</label>
        <input id="f_type" type="radio" name="f_type" value="1">面長
        <input id="f_type" type="radio" name="f_type" value="2">丸顔
        <br>
    <li>
     </li>
        <li class="f_color">
        <label for="f_color">■肌の色</label>
        <input id="f_color" type="radio" name="f_color" value="1">色白
        <input id="f_color" type="radio" name="f_color" value="2">色黒
        <br>
    <li>
     </li>
        <li class="neck">
        <label for="neck">■首の長さ</label>
        <input id="neck" type="radio" name="neck" value="1">首が長い
        <input id="neck" type="radio" name="neck" value="2">首が長い
        <br>
    <li>
        <input type="submit" id="button" name="button" value=" 検索 ">
    </li>
</ul>
</form>
  </div>
  </section>
<form action="Q1Servlet" method="get">
                        <div class="button2">
                            <div class="a">髪型診断はこちら⬇️</div>
                            <button type="submit" value="髪型診断" class="btn btn-primary form-submit">髪型診断</button>
                        </div>
</form>
<form action="UserDeleteServlet" method="get">
                        <div class="button2">
                            <div class="a">アカウント削除はこちら⚠️️</div>
                            <button type="submit" value="アカウント削除" class="btn btn-primary form-submit">アカウント削除</button>
                            <input type="hidden" name="login_id" value="${user.loginId}">
                        </div>
</form>


</body>
<footer>
    <div id="button">footer</div>
    <div id="container">
        <div id="cont">
            <div class="copyright">
                <p>© 2020 - Organisation</p>
            </div>
            <div class="sns">
                <a href="#" class="insta"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></a>
                <a href="#" class="tw"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></a>
                <a href="#" class="fb"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</footer>

</html>
