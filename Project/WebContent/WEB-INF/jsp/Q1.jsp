<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet" href="./css/Q1.css">

<body>
	<div class="overflow">
		<section class="panels">
			<form action="Q1Servlet" method="post">
			<input type="hidden" name="id" value="1">
				<article class="panels__side panels__side--left">
					<div class="panels__side panels__side--inner ">

						<!-- 遷移先リンク -->
						<button class="button1" type="submit">
							<h1 class="panels__headline">男性</h1> <svg
								class="arrow arrow--left" width="40" height="40"
								viewBox="0 0 24 24" >
							<path d="M0 0h24v24h-24z" fill="none" />
							<path
									d="M20 11h-12.17l5.59-5.59-1.42-1.41-8 8 8 8 1.41-1.41-5.58-5.59h12.17v-2z" /></svg>
						</button>
					</div>
				</article>
			</form>
			<form action="Q1Servlet" method="post">
			<input type="hidden" name="id" value="2">
				<article class="panels__side panels__side--right">
					<div class="panels__side panels__side--inner">

						<!-- 遷移先リンク -->
						<button class="button1" type="submit">
							<h1 class="panels__headline">女性</h1> <svg
								class="arrow arrow--left" width="40" height="40"
								viewBox="0 0 24 24" >
                <path d="M0 0h24v24h-24z" fill="none" />
                <path
									d="M12 4l-1.41 1.41 5.58 5.59h-12.17v2h12.17l-5.58 5.59 1.41 1.41 8-8z" />
              </svg>
						</a>
					</div>
				</article>
			</form>
		</section>
	</div>
</body>
