
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta 　http-equiv="content-type" content="text/html; charset=utf-8">
<title>ログイン</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" href="./css/login.css">
</head>





<body>
<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>


	<hr>
	<div align="center">
		<table border="0">
			<form action="LoginServlet" method="post">
                  <section class="card">

  <div class="card-content">
    <h1 class="card-title">ログイン画面</h1>
    <ul>
        <li>
            <label>ユーザID</label><br>
					<input type="text" name="loginId" value="" size="24">
        </li>
        <li>
        <label>パスワード</label><br>
					<input type="text" name="password" value="" size="24">
        </li>
        <br>
        <li>
        <input type="submit" value="ログイン">
        </li>

      </ul>
  </div>

</section>
			</form>
				<form action="UserNewServlet" method="get">
            <td colspan="2"><input type="submit" value="新規登録"><p style="text-align:center"></td>
            </form>
		</table>
	</div>
</body>
<footer>
	<div id="button">footer</div>
	<div id="container">
	<div id="cont">
		<div class="copyright">
			<p>© 2020 - Organisation</p>
		</div>
		<div class="sns">
			<a href="#" class="insta"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></a>
			<a href="#" class="tw"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></a>
			<a href="#" class="fb"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a>
		</div>
	</div>
	</div>
</footer>
</html>