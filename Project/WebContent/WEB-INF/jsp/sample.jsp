<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="./css/sample.css">

<body>
<div class="overflow">
  <section class="panels">

      <article class="panels__side panels__side--left">
        <div class="panels__side panels__side--inner ">
        　
        　<!-- 遷移先リンク -->
          <a class ="Link" href="../@@@@.html">

        <h1 class="panels__headline">Noam Chomsky</h1>
        <svg class="arrow arrow--left" width="40" height="40" viewBox="0 0 24 24"><path d="M0 0h24v24h-24z" fill="none"/><path d="M20 11h-12.17l5.59-5.59-1.42-1.41-8 8 8 8 1.41-1.41-5.58-5.59h12.17v-2z"/></svg>
              </a>
      </div>
      </article>

      <article class="panels__side panels__side--right">
          <div class="panels__side panels__side--inner">

        　<!-- 遷移先リンク -->
          <a class ="Link" href="../@@@@.html">

            <h1 class="panels__headline">Buzz Aldrin</h1>
              <svg class="arrow arrow--right" width="40" height="40" viewBox="0 0 24 24" >
                <path d="M0 0h24v24h-24z" fill="none"/>
                <path d="M12 4l-1.41 1.41 5.58 5.59h-12.17v2h12.17l-5.58 5.59 1.41 1.41 8-8z"/>
              </svg>
          </a>
          </div>
        </article>

    </section>
</div>
</body>
