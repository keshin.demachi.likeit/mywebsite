<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" href="./css/test.css">
<link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" href="./css/index.css">
        <link rel="stylesheet" href="./css/header.css">
<title>髪型情報更新</title>

</head>
<body>


	<header>
		<h1 class="headline">
			<a class="">H.D.S ~Hair Diagnosis Site~</a>
		</h1>
		<ul class="nav-list">
			<li class="nav-list-item"></li>

			<a href="LogoutServlet" class="btn-flat-horizontal-border"> <span>ログアウト</span>
			</a>
		</ul>
	</header>

	<h1>髪型更新</h1>
	<hr>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<div align="center">
		<table border="0">
			<form action="HairUpdateServlet" enctype="multipart/form-data"
				method="post">
				<input type="file" name="file" /><br />
			<tr>
				<th>髪型名</th>
				<td><input type="text" name="h_name" value="${h_name}"
					size="24"> <input type="hidden" name="img" value="${img}">
					<input type="hidden" name="img_id" value="${img_id}"></td>
			</tr>

			<td colspan="2"><input type="submit" value="更新">
				<p style="text-align: center"></td>



			</form>
		</table>
	</div>
</body>
</html>