<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="header.css">
    <link rel="stylesheet" href="footer.css">
  <title>ユーザ情報更新</title>
</head>
<body>


  <header>
        <h1 class="headline">
            <a class="">H.D.S ~Hair Diagnosis Site~</a>
        </h1>
        <ul class="nav-list">
            <li class="nav-list-item">

            </li>
            <a href="#" class="btn-flat-horizontal-border">
                <span>マイページ</span>
            </a>
            <a href="LogoutServlet" class="btn-flat-horizontal-border">
                <span>ログアウト</span>
            </a>
        </ul>
    </header>
     <h1>ユーザ更新</h1>
                                   <hr>
                                   <c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
                                   <div align="center">
                                    <table border="0">
                                     <form action="UpdateServlet"method="post">

   <tr>
     <th>
        ログインID    ${loginId}                                                                                                                                           </th>
      <td>
 <input type="hidden" name="id"value="${id}  "size="24">
  <input type="hidden" name="loginId"value="${}  "size="24">
        </td>
        </tr>
       <tr>
        <th>
        パスワード
        </th>
        <td>
           <input type="password"name=""value=""size="24">
           </td>
            </tr>
             <tr>
        <th>
        パスワード(確認)
        </th>
        <td>
           <input type="password"name=""value=""size="24">
           </td>
            </tr>
             <tr>
        <th>
        ユーザ名
        </th>
        <td>
           <input type="text"name="name"value="${}"size="24">
           </td>
            </tr>
              <tr>
        <th>
        生年月日
        </th>
        <td>
           <input type="text"name="date"value="${}"size="24">
           </td>
            </tr>
           <tr>
<th>
       性別
        </th>
        <td>
           <input type="text"name="name"value="${}"size="24">
           </td>
            </tr>
              <tr>
           <tr>
            <td colspan="2">
                <input type="submit"value="更新">
               </td>
                 </tr>
 </form>
     </table>
    </div>
</body>

    <footer>
	<div id="button">footer</div>
	<div id="container">
	<div id="cont">
		<div class="copyright">
			<p>© 2020 - Organisation</p>
		</div>
		<div class="sns">
			<a href="#" class="insta"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></a>
			<a href="#" class="tw"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></a>
			<a href="#" class="fb"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a>
		</div>
	</div>
	</div>
</footer>
</html>
