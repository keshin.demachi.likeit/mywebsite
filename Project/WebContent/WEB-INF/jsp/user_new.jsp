<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="./css/footer.css">
<link rel="stylesheet" href="./css/header.css">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<header>
		<h1 class="headline">
			<a class="">H.D.S ~Hair Diagnosis Site~</a>
		</h1>
		<ul class="nav-list">
			<li class="nav-list-item"></li>

			<a href="#" class="btn-flat-horizontal-border"> <span>ログアウト</span>
			</a>
		</ul>
	</header>
	<h1>ユーザ新規登録</h1>
	<hr>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<table border="0">
		<div align="center">
			<th>新規登録</th>
			<tr>
				<form action="UserNewServlet" method="post">
					<th>ログインID</th>
					<td><input type="text" name="login_id" value="" size="24">
					</td>
			</tr>
			<tr>
				<th>パスワード</th>
				<td><input type="password" name="password" value="" size="24">
				</td>
			</tr>


			<tr>
				<th>ユーザ名</th>
				<td><input type="username" name="user_name" value="" size="24">
				</td>
			</tr>
			<tr>
				<th>性別</th>
				<td><input type="gender" name="gender" value="" size="24">
				</td>
			</tr>
			<tr>
				<th>生年月日</th>
				<td><input type="date" name="user_d" value="" size="24">
				</td>

			</tr>
		<td colspan="2">
			<button type="submit" value="登録" class="btn btn-primary form-submit">登録</button>
		</td>
		</form>
		</div>


		<form action="LoginServlet" method="get">
		<td colspan="1">
			<button type="submit" value="戻る" class="btn btn-primary form-submit">戻る</button>
		</td>
		</form>
		<footer>
			<div id="button">footer</div>
			<div id="container">
				<div id="cont">
					<div class="copyright">
						<p>© 2020 - Organisation</p>
					</div>
					<div class="sns">
						<a href="#" class="insta"><i class="fa fa-instagram fa-fw"
							aria-hidden="true"></i></a> <a href="#" class="tw"><i
							class="fa fa-twitter fa-fw" aria-hidden="true"></i></a> <a href="#"
							class="fb"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</footer>
	</table>
</body>
</html>